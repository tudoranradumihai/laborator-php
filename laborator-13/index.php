<?php
	$connection = mysqli_connect("localhost","root","","laborator-13");
	$query = "SELECT * FROM products";
	$result = mysqli_query($connection,$query);
	mysqli_close($connection);
?>
<!DOCTYPE html>
<html>
	<head>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	</head>
	<body>
		<div class="container">
			<h1>Products</h1>
			<div class="btn-group" role="group">
				<a href="new.php">
					<button type="button" class="btn btn-default">Add New Product</button>
			  	</a>
			</div>
			<?php if (mysqli_num_rows($result)>0) { ?>
			<table class="table table-striped">
				<thead>
					<td>ID</td>
					<td>Name</td>
					<td>Price</td>
					<td>Stock</td>
				</thead>
				<?php while($line = mysqli_fetch_array($result,MYSQLI_ASSOC)) { ?>
				<tr>
					<td><?php echo $line["id"]; ?></td>
					<td><?php echo $line["name"]; ?></td>
					<td><?php echo $line["price"]; ?></td>
					<td><?php echo $line["stock"]; ?></td>
					<td><a href="show.php?id=<?php echo $line["id"] ?>">Show</a></td>
					<td><a href="edit.php?id=<?php echo $line["id"] ?>">Edit</a></td>
					<td><a href="delete.php?id=<?php echo $line["id"] ?>">Delete</a></td>
				</tr>
				<?php } ?>
			</table>
			<?php } ?>
		</div>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
	</body>
</html>