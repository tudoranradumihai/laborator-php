<?php
	if (array_key_exists("id",$_GET)){
		if ($_GET["id"]!=""){
			$connection = mysqli_connect("localhost","root","","laborator-13");
			$result = mysqli_query($connection, "SELECT * FROM products WHERE id=".intval($_GET["id"]));
			$result = mysqli_fetch_array($result,MYSQLI_ASSOC);
			mysqli_close($connection);
		}
	}
?>
<!DOCTYPE html>
<html>
	<head>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	</head>
	<body>
		<div class="container">
			<h1>Edit Product</h1>
			<form method="POST" action="update.php">
				<input type="hidden" name="id" value="<?php echo $result["id"]?>">
				<div class="form-group">
					<label for="name">Name</label>
					<input name="name" type="text" class="form-control" id="name" placeholder="Name" value="<?php echo $result["name"] ?>">
				</div>
				<div class="form-group">
					<label for="description">Description</label>
					<textarea class="form-control" rows="3" id="description" name="description"><?php echo $result["description"] ?></textarea>
				</div>
				<div class="form-group">
					<label for="price">Price</label>
					<input name="price" type="text" class="form-control" id="price" placeholder="0.00" value="<?php echo $result["price"] ?>">
				</div>
				<div class="form-group">
					<label for="stock">Stock</label>
					<input name="stock" type="text" class="form-control" id="stock" placeholder="0" value="<?php echo $result["stock"] ?>">
				</div>
				<button type="submit" class="btn btn-default">Submit</button>
			</form>
		</div>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
	</body>
</html>