<?php
	if (array_key_exists("id", $_GET)){
		if ($_GET["id"]!=""){
			$connection = mysqli_connect("localhost","root","","laborator-13");
			$query = "SELECT * FROM products WHERE id=".intval($_GET["id"]);
			$result = mysqli_query($connection,$query);
			$result = mysqli_fetch_array($result,MYSQLI_ASSOC);
			mysqli_close($connection);
			$flag = true;
		} else {
			$flag = false;
		}
	} else {
		$flag = false;
	}
	
	if (!$flag){
		echo '<meta http-equiv="refresh" content="0; url=index.php" />';	
	}
?>
<!DOCTYPE html>
<html>
	<head>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	</head>
	<body>
		<div class="container">
			<h1>Show Product - <?php echo $result["name"]; ?></h1>
			<div class="btn-group" role="group">
				<a href="new.php">
					<button type="button" class="btn btn-default">Add New Product</button>
			  	</a>
			</div>
			<p><?php echo nl2br($result["description"]) ?></p>
		</div>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
	</body>
</html>