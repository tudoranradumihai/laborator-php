<?php
$array = array(
	"firstname" => "Radu",
	"lastname" => "Tudoran"
);


// create class
Class User {
	public $firstname;
	public $lastname;
}

// create object
$user = new User();
// add values to object properties
$user->firstname = $array["firstname"];
$user->lastname  = $array["lastname"];

// FACTORY

Class User {
	public $firstname;
	public $lastname;
	public function __construct($firstname,$lastname){
		$this->firstname = $firstname;
		$this->lastname  = $lastname;
	}
}

$user = new User($array["firstname"],$array["lastname"]);