<?php 

Class DefaultController extends Controller {

	public function defaultAction(){

		include "Views/Default/default.php";
	}

	public function page404Action(){
		include "Views/Default/page404.php";
	}

}