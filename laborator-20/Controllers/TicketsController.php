<?php

Class TicketsController extends Controller {

	public function defaultAction(){
		self::listAction();
	}

	public function listAction(){

	}

	public function newAction(){
		include "Views/Tickets/new.php";
		if(array_key_exists("tickets-create-errors",$_SESSION)){
			unset($_SESSION["tickets-create-errors"]);
		}
		if(array_key_exists("tickets-create-values",$_SESSION)){
			unset($_SESSION["tickets-create-values"]);
		}
	}


	public function createAction(){
		if($_SERVER["REQUEST_METHOD"]=="POST"){
			$validation = true;

			$errors = array();
			$values = array();

			$requiredFields = array("subject","message");
			foreach ($requiredFields as $field){
				if (trim($_POST[$field]=="")){
					$validation = false;
					array_push($errors, "The field '$field' is required.");
				} else {
					$values[$field] = $_POST[$field];
				}
			}

			if ($validation){
				$ticket = new Ticket(NULL,$_POST["subject"],$_POST["message"]);
				Ticket::insert($ticket);
				header("Location: index.php?C=Tickets&A=list");
			} else {
				$_SESSION["tickets-create-errors"] = $errors;
				$_SESSION["tickets-create-values"] = $values;
				header("Location: index.php?C=Tickets&A=new");
			}
		} else {
			header("Location: index.php?C=Tickets&A=new");
		}
	}

}