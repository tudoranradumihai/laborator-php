<form method="POST" action="index.php?C=Tickets&A=create">
<?php
	if(array_key_exists("tickets-create-errors",$_SESSION)){
		foreach($_SESSION["tickets-create-errors"] as $error){
			echo "<div class=\"alert alert-danger\" role=\"alert\">$error</div>";
		}
	}
?>
	<div class="form-group">
		<label for="subject">Subject</label>
		<input type="text" class="form-control" id="subject" placeholder="Subject" name="subject" value="<?php echo checkField("tickets-create-values","subject"); ?>">
	</div>
	<div class="form-group">
		<label for="message">Message</label>
		<textarea class="form-control" id="message" name="message" rows="3"><?php echo checkField("tickets-create-values","message"); ?></textarea>
	</div>
	<button type="submit" class="btn btn-default">Create Ticket</button>
</form>