<?php if (array_key_exists("user", $_COOKIE)) { ?>
<div class="row">
	<div class="col-md-4 col-md-offset-4">
		You are logged in as <a href="index.php?C=Users&A=show"><?php echo $_SESSION["user"]["emailaddress"] ?>
	</div>
</div>
<?php } else { ?>
<div class="row">
	<div class="col-md-4 col-md-offset-4">
		<h1>Login</h1>
		<?php
			if (array_key_exists("users-connect-errors", $_SESSION)){
				foreach ($_SESSION["users-connect-errors"] as $error){
					echo '<div class="alert alert-danger" role="alert">'.$error.'</div>';
				}
			}
		?>
		<form method="POST" action="index.php?C=Users&A=connect">
			<div class="form-group">
				<label for="emailaddress">Email address</label>
				<input type="email" class="form-control" id="emailaddress" name="emailaddress" placeholder="Email">
			</div>
			<div class="form-group">
				<label for="password">Password</label>
				<input type="password" class="form-control" id="password" name="password" placeholder="Password">
  </div>
  			<div class="pull-right">
  				<button type="submit" class="btn btn-default">Submit</button>
  			</div>
		</form>
	</div>
</div>
<?php } ?>

