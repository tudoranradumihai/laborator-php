<?php

function __autoload($class){
	$folders = array(
		"Controllers",
		"Models",
		"Helpers"
	);
	foreach($folders as $folder){
		$filePath = $folder."/".$class.".php";
		if(file_exists($filePath)){
			include $filePath;
		}
	}
}