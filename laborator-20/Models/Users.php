<?php

Class Users {

	public $id;
	public $firstname;
	public $lastname;
	public $birthdate;
	public $address;
	public $zip;
	public $city;
	public $country;
	public $phone;
	public $emailaddress;
	public $password;

	public function __construct($id,$firstname,$lastname,$birthdate,$address,$zip,$city,$country,$phone,$emailaddress,$password){

		$this->id = $id;
		$this->firstname = $firstname;
		$this->lastname = $lastname;
		$this->birthdate = $birthdate;
		$this->address = $address;
		$this->zip = $zip;
		$this->city = $city;
		$this->country = $country;
		$this->phone = $phone;
		$this->emailaddress = $emailaddress;
		$this->password = $password;

	}

	public static function insert($user){
		$keys = array();
		$values = array();
		foreach ($user as $key => $value){
			if($key!="id"){
				array_push($keys,$key);
				array_push($values,'"'.$value.'"');
			}
		}
		$query = "INSERT INTO users (".implode(",",$keys).") VALUES (".implode(",",$values).");";
		$database = new Database();
		return $database->query($query);
	}

	public static function checkCredentials($emailaddress,$password){
		$query = "SELECT * FROM users WHERE emailaddress LIKE \"$emailaddress\" AND password LIKE \"$password\";";
		$database = new Database();
		$result = $database->query($query);
		if(mysqli_num_rows($result)){
			$result = mysqli_fetch_array($result,MYSQLI_ASSOC);
			$user = new Users($result["id"],$result["firstname"],$result["lastname"],$result["birthdate"],$result["address"],$result["zip"],$result["city"],$result["country"],$result["phone"],$result["emailaddress"],$result["password"]);
			return $user;
		} else {
			return NULL;
		}
	}

	public static function show($id){
		$query = "SELECT * FROM users WHERE id=$id;";
		$database = new Database();
		$result = $database->query($query);
		if(mysqli_num_rows($result)){
			$result = mysqli_fetch_array($result,MYSQLI_ASSOC);
			$user = new Users($result["id"],$result["firstname"],$result["lastname"],$result["birthdate"],$result["address"],$result["zip"],$result["city"],$result["country"],$result["phone"],$result["emailaddress"],$result["password"]);
			return $user;
		} else {
			return NULL;
		}
	}

	public static function edit($user){

		$statement = array();
		foreach ($user as $key => $value){
			if ($key!="id" && $value!=NULL){
				array_push($statement,"$key = '$value'");
			}
		}
		$query = "UPDATE users SET ".implode(", ",$statement)." WHERE id=".$user->id;
		$database = new Database();
		return $database->query($query);
	}
}