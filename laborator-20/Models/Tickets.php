<?php

Class Tickets {
	public $id;
	public $subject;
	public $message;

	public function __construct($id,$subject,$message){
		$this->id = $id;
		$this->subject = $subject;
		$this->message = $message;
	}

	public static function insert($ticket){
		$keys = array();
		$values = array();

		foreach ($ticket as $key => $value){
			if($value){
				array_push($keys,$key);
				array_push($values,'"'.$value.'"');
			}
		}

		$query = "INSERT INTO tickets (".implode(",",$keys).") VALUES (".implode(",",$values).");";
		$database = new Database();
		$database->query($query);
	}
}