<?php

Class C1 {
	public $p1;
	public function m1(){
		echo $this->p1;
	}
}
/*
Class C3 {
	public $p1;
	public $p2;
	public function m1(){
		echo $this->p1;
	}
}
*/

Class C3 extends C1 {
	public $p2;
	public function m1(){
		echo "C3-".$this->p1;
	}
}

$o1 = new C3();
$o1->p1 = 1;	 
$o1->p2 = 2;