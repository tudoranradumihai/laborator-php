<?php
/*
C1
p1 public
p2 protected
p3 private
m1 seteaza p1
m2 returneaza p1
*/
Class C1 {
	public $p1;
	protected $p2;
	private $p3;
	public function m1($p){
		echo __METHOD__;
		$this->p1=$p;
	}
	public function m2(){
		echo __METHOD__;
		return $this->p1;
	}
} 

//$o1 = new C1();
//$o1->m1(1);

/*
C2 extends C1
m1 seteaza p2
m2 returneaza p2
*/
Class C2 extends C1{
	public function m1($p){echo __METHOD__;$this->p2=$p;}
	public function m2(){echo __METHOD__;return $this->p2;}
}
/*
C3 extends C2
m3 seteaza p1 prin metoda
m4 returneaza p1 prin metoda
m5 seteaza p2 prin metoda
m6 returneaza p2 prin metoda
*/
Class C3 extends C2{
	public function m3($p1){
		echo __METHOD__;
		self::m1($p1);
	}
}

$o3 = new C3();
$o3->m3(1);
