<?php

Class C1 {
	public $p1;
	protected $p2;
	private $p3;

	public function m1(){
		echo $this->p1; // OK
		echo $this->p2; // OK
		echo $this->p3; // OK
	}

	protected function m3(){
		echo "PROTECTED";
	}

}

$o1 = new C1();
$o1->p1 = 1; // OK
$o1->p2 = 2; // NOT OK
$o1->p3 = 3; // NOT OK

Class C2 extends C1{

	public function m1(){
		echo "OVERWRITE";
	}

	public function m2(){
		echo $this->p1; // OK
		echo $this->p2; // OK
		echo $this->p3; // NOT OK

		self::m1();// apelare metoda din clasa
		parent::m1(); // apelare metoda din clasa parinte
		self::m3();

	}

}

$o2 = new C2();
$o2->p1 = 1; // OK
$o2->p2 = 2; // NOT OK
$o2->p3 = 3; // NOT OK