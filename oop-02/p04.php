<?php
/*
$connection = mysqli_connect("localhost","root","","DB");
$result = mysqli_query($connection,$query);
mysqli_close($connection);
*/
Class Database {

	private $connection;

	public function __construct(){
		$this->connection = mysqli_connect("localhost","root","","DB");
	}

	public function executeQuery($query){
		return mysqli_query($this->connection,$query);
	}

	public function __destruct(){
		mysqli_close($this->connection);
	}

}