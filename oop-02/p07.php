<?php

Class FileSystem {

	protected $fileContent;

	public function __construct($file,$type){
		$this->fileContent = fopen($file,$type);
	}

	public function __destruct(){
		fclose($this->fileContent);
	}

}

Class Append extends FileSystem {

	const TYPE = "a+";

	public function __construct($file){
		parent::__construct($file,TYPE);
	}

	public function setLine($line){
		fputs($this->fileContent,$line.PHP_EOL);
	}

}

Class Write extends FileSystem {

	const TYPE = "w";

	public function __construct($file){
		parent::__construct($file,TYPE);
	}

	public function setLine($line){
		fputs($this->fileContent,$line.PHP_EOL);
	}

}

Class Read extends FileSystem {

	const TYPE = "r";

	public function __construct($file){
		parent::__construct($file,TYPE);
	}

	public function getAll(){
		$array = array();
		while($line = fgets($this->fileContent)){
			array_push($array,$line);
		}
		return $array;
	}

	public function getLine(){
		return fgets($this->fileContent);
	}

}

$o = new Read("file.txt");
$result = $o->getAll();