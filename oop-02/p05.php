<?php
/*
Clasa C1
cu p1 public
p2 privat
metoda m1 care seteaza valoarea p2
metoda m2 care returneaza valoarea p2
*/

Class C1 {
	public $p1;
	private $p2;
	public function m1($p){
		$this->p2 = $p;
	}
	public function m2(){
		return $this->p2;
	} 
}


/*
Clasa C2
p1 protejat
p2 protejat
metoda m1 care seteaza valoarea p1
metoda m2 care returneaza valoarea p1
*/ 

Class C2 {
	protected $p1;
	protected $p2;
	public function m1($p){
		$this->p1 = $p;
	}
	public function m2(){
		return $this->p1;
	}
}

/*
clasa C3 copil al C2
metoda m1 care seteaza valoarea p2 din c2
metoda m2 care returneaza valoarea p2 din c2
metoda m3 care seteaza valoarea p1 folosind metoda clasei c2
metoda m4 care returneaza valoarea p1 folosind metoda clasei c2
*/

Class C3 extends C2 {
	public function m1($p){
		$this->p2 = $p;
	}
	public function m2(){
		return $this->p2;
	}
	public function m3($p){
		parent::m1($p);
	}
	public function m4(){
		return parent::m2();
	}
}