<?php

/*
Definirea clasei C1
*/
Class C1 {
	/*
	definirea proprietatilor publice p1, p2 si p3
	*/
	public $p1;
	public $p2;
	public $p3;

}

/*
instantierea obiectului din clasa C1
*/
$o1 = new C1();
$o1->p1 = 1;
$o1->p2 = 2;

Class C2 {

	public $p1;
	private $p2;

	public function m1($parametru){
		$this->p2 = $parametru;
	}

}

$o2 = new C2();
$o2->p1 = 1;
$o2->p2 = 2; // AICI SE RETURNEAZA EROARE
$o2->m1(2);

Class C3 {

	private $p1;

	public function __construct($parametru){
		$this->p1 = $parametru;
	}


}

$o3 = new C3(1);