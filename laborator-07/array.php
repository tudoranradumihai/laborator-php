<?php

// declarare variabila ca si array gol
$lista1 = array();
// adaugare valori fara a conta cheile lor
$lista1[] = "prima valoare";
$lista1[] = "a 2 valoare";
$lista1[] = "a 3 valoare";
$lista1[] = "a 4 valoare";
//var_dump($lista1);
$lista2 = array("prima valoare","a 2 valoare","a 3 valoare");
//var_dump($lista2);
$lista2 = array("a 3 valoare","a 4 valoare");
$lista2[] = "alta valoare";
$lista2[1] = "o noua valoare";
$lista2["cheia_suprema"] = "adevarat";
$lista2[] = "cea mai noua valoare $$$";
//var_dump($lista2);
$user = array(
	"firstname" 	=> "Radu",
	"lastname"		=> "Tudoran"
);

//var_dump($user);
echo "<br />";
//var_dump(count($user));
echo "<br />";
//var_dump(array_values($user));
echo "<br />";
//var_dump(array_keys($user));
echo "<br />";
$a = array("fn","ln");
$b = array("R","T");
$c = array_combine($a, $b);
//var_dump($c);
$arr = array(1,2,3,4,4,5,6,7,7,8,9,10,10,11,4,6);
//var_dump(array_values(array_unique($arr)));

//echo in_array("Radu",$user);
echo "<br />";
//echo array_key_exists("firstname", $user);

$key = "fistname";
//if (array_key_exists($key, $user))
//echo $user[$key];


$array1 = array(1,2,3,4);
$array2 = array(1,2,3,4);

//var_dump(array_merge($array1,$array2));

$array1[] = 5;
array_push($array1,6);
var_dump($array1);

$array1=array_values($array1);
echo $array1[count($array1)-1];
unset($array1[count($array1)-1]);
var_dump($array1);
echo array_pop($array1);
var_dump($array1);

$fructe = array("mere","pere","pepeni","prune");

$ultimul = array_pop($fructe);
echo implode(", ",$fructe)." si ".$ultimul;