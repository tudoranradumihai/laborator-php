<?php
session_start();

if ($_SERVER["REQUEST_METHOD"]=="POST"){
	$validation = true;
	$errors = array();

	if (trim($_POST["firstname"])==""){
		array_push($errors,"Field firstname is required.");
		$validation = false;
	}
	if (trim($_POST["lastname"])==""){
		array_push($errors,"Field lastname is required.");
		$validation = false;
	}

	if ($validation){
		// aici se executa insert in db
	} else {
		$_SESSION["errors"] = $errors;
		echo '<meta http-equiv="refresh" content="0; url=index.php?page=new" />';
	}
} else {
	echo '<meta http-equiv="refresh" content="0; url=index.php?page=new" />';
}