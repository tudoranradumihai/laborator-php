<?php

/*
domain.com/index.php?C=Users&A=show&ID=1
domain.com/Users/show/1

URL::show(controller,action,id);
*/

Class URL {

	public static function create($controller,$action,$identifier){
		$configuration = require "Configuration/Configuration.php";
		$link = "";
		if($configuration["GENERAL"]["ABSOLUTE_PATH"]){
			$link .= $configuration["GENERAL"]["DOMAIN"];
		}
		if($configuration["GENERAL"]["REWRITE_MODE"]){
			$link .= "$controller/$action";
			if($identifier!=NULL){
				$link .= "/$identifier";
			}

		} else {
			$link .= "index.php?C=$controller&A=$action";
			if($identifier!=NULL){
				$link .= "&ID=$identifier";
			}
		}


		
		return $link;
	}

	public static function show($controller,$action,$identifier = NULL){
		echo self::create($controller,$action,$identifier);
	}

	public static function redirect($controller,$action,$identifier = NULL){
		header("Location: ".self::create($controller,$action,$identifier));
	}

}