<?php

// https://www.w3schools.com/php/php_mysql_intro.asp

Class Database {

	private $connection;

	public function __construct(){
		$configuration = require "Configuration/Confiuration.php";
		$this->connection = new mysqli(
			$configuration["DATABASE"]["HOSTNAME"],
			$configuration["DATABASE"]["USERNAME"],
			$configuration["DATABASE"]["PASSWORD"],
			$configuration["DATABASE"]["DATABASE"]
		);
	}

	public function query($query){
		return $this->connection->query($query);
	}

	public function __destruct(){
		$this->connection->close();
	}

}