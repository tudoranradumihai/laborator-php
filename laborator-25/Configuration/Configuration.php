<?php

return array(
	"GENERAL" => array(
		"DOMAIN" => "http://localhost/laborator-25/",
		"ABSOLUTE_PATH" => TRUE,
		"REWRITE_MODE" => TRUE
	),
	"DATABASE" => array(
		"HOSTNAME" => "localhost",
		"USERNAME" => "root",
		"PASSWORD" => "",
		"DATABASE" => "",
	)
);