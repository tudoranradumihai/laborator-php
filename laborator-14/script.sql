DROP TABLE IF EXISTS cities;
DROP TABLE IF EXISTS countries;
DROP TABLE IF EXISTS continents;

CREATE TABLE continents(
	id INT(11) PRIMARY KEY AUTO_INCREMENT,
	name VARCHAR(255) NOT NULL
);

CREATE TABLE countries (
	id INT(11) PRIMARY KEY AUTO_INCREMENT,
	name VARCHAR(255) NOT NULL,
	continent INT(11),
	FOREIGN KEY (continent) REFERENCES continents(id)
);

CREATE TABLE cities (
	id INT(11) PRIMARY KEY AUTO_INCREMENT,
	name VARCHAR(255) NOT NULL,
	country INT(11) NOT NULL,
	FOREIGN KEY (country) REFERENCES countries(id)
);	


SELECT cities.id as 'id', cities.name AS 'city', countries.name AS 'country'
FROM cities 
INNER JOIN countries
	ON cities.country = countries.id
WHERE cities.id=1;