<!DOCTYPE html>
<html>
	<head>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	</head>
	<body>
		<div class="container">
			<h1>Google Earth (Indian Version)</h1>
			<div class="btn-group" role="group" aria-label="...">
				<button type="button" class="btn btn-default">+ Add City</button>
				<a href="new-country.php">
					<button type="button" class="btn btn-default">+ Add Country</button>
				</a>
				<a href="new-continent.php">
					<button type="button" class="btn btn-default">+ Add Continent</button>
				</a>
			</div>
		</div>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
	</body>
</html>