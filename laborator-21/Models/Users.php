<?php

Class Users {

	public $id;
	public $firstname;
	public $lastname;
	public $birthdate;
	public $address;
	public $zip;
	public $city;
	public $country;
	public $phone;
	public $emailaddress;
	public $password;

	public function __construct($id,$firstname,$lastname,$birthdate,$address,$zip,$city,$country,$phone,$emailaddress,$password){

		$this->id = $id;
		$this->firstname = $firstname;
		$this->lastname = $lastname;
		$this->birthdate = $birthdate;
		$this->address = $address;
		$this->zip = $zip;
		$this->city = $city;
		$this->country = $country;
		$this->phone = $phone;
		$this->emailaddress = $emailaddress;
		$this->password = $password;

	}





}