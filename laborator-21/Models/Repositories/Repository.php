<?php

Abstract Class Repository {

	protected $table;
	protected $database;

	public function __construct(){
		$this->database = new Database();
		$this->table = strtolower(str_replace("Repository","",get_called_class()));
		$this->model = str_replace("Repository","",get_called_class());
	}

	public function insert($user){
		$keys = array();
		$values = array();
		foreach ($user as $key => $value){
			if($key!="id"){
				array_push($keys,$key);
				array_push($values,'"'.$value.'"');
			}
		}
		$query = "INSERT INTO $this->table (".implode(",",$keys).") VALUES (".implode(",",$values).");";
		return $this->database->query($query);
	}

		public function show($id){
		$query = "SELECT * FROM $this->table WHERE id=$id;";
		$result = $this->database->query($query);
		if(mysqli_num_rows($result)){
			$result = mysqli_fetch_array($result,MYSQLI_ASSOC);
			$object = new $this->model();
			foreach($object as $key => $value){
				$object->$key = $result[$key];
			}
			return $object;
		} else {
			return NULL;
		}
	}

}