$(document).ready(function(){

	$("#newproduct").validate({
		rules: {
			name: "required",
			price: {
				required: true,
				number: true
			},
			stock: {
				required: true,
				number: true
			}
		},
		messages: {
			name: "The product name is required!",
			price: {
				required: 'The product price is required!',
				number: 'You have not entered a valid number!'
			},
			stock: {
				required: 'The product stock is required!',
				number: 'You have not entered a valid number!'
			}
		},
		submitHandler: function(form) {
	      form.submit();
	    }
	});

});