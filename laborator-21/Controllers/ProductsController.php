<?php

Class ProductsController extends Controller {

	private $productsRepository;

	public function __construct(){
		$this->productsRepository = new ProductsRepository();
	}

	public function defaultAction(){

	}

	public function newAction(){
		include "Views/Products/new.php";
	}

	public function createAction(){
		// homework: implement PHP validation
	}

	public function showAction(){
		if (array_key_exists("ID", $_GET)){
			if (is_numeric($_GET["ID"])){
				$product = $this->productsRepository->show($_GET["ID"]);
				if (!$product){
					//header("Location: index.php?C=Default&A=page404");
				}
			} else {
				//header("Location: index.php?C=Default&A=page404");
			}
		} else {
			//header("Location: index.php?C=Default&A=page404");
		}
		include "Views/Users/show.php";
	}

}