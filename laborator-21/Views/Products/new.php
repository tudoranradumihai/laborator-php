<form method="POST" action="index.php?C=Products&A=create" id="newproduct">
	<div class="row">
		<div class="col-md-12">
			<h1>New Product</h1>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus viverra sed mi eget interdum. Quisque ut eros ut leo posuere vestibulum. Donec magna velit, interdum ac velit eu, iaculis placerat elit. Sed quis sollicitudin nisi, sed posuere dui. Sed malesuada elit nunc, sit amet ultrices felis finibus non. Proin pretium elit vel varius varius. Praesent ornare, sem sed ullamcorper congue, ex neque commodo ex, id ultricies ante turpis vehicula eros. Ut tincidunt libero lorem, nec faucibus lectus sodales sit amet. Sed lectus nibh, pharetra quis lacus eu, efficitur mollis turpis. Nunc dictum libero fringilla, dictum turpis porttitor, congue sem. Aliquam ultricies tristique dui sit amet dapibus.</p>
		</div>
		<div class="col-md-6">
			<div class="form-group">
				<label for="name">Name:</label>
				<input type="text" class="form-control" id="name" placeholder="TV" name="name">
			</div>
		</div>
		<div class="col-md-6">
			<div class="form-group">
				<label for="image">Image:</label>
				<input type="file" class="form-control" id="image" name="image">
			</div>
		</div>
		<div class="col-md-12">
			<div class="form-group">
				<label for="description">Description:</label>
				<textarea class="form-control" rows="5" id="description" name="description"></textarea>
			</div>
		</div>
		<div class="col-md-6">
			<div class="form-group">
				<label for="price">Price:</label>
				<div class="input-group">
					<div class="input-group-addon">EUR</div>
					<input type="text" class="form-control" id="price" name="price" placeholder="9.99">
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="form-group">
				<label for="promotionprice">Promotion Price:</label>
				<div class="input-group">
					<div class="input-group-addon">EUR</div>
					<input type="text" class="form-control" id="promotionprice" name="promotionprice" placeholder="9.99">
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="form-group">
				<label for="stock">Stock:</label>
				<input type="text" class="form-control" id="stock" name="stock" placeholder="0">
			</div>
		</div>
		<div class="col-md-12">
			<div class="pull-right">
				<button type="submit" class="btn btn-default">Create New Product</button>
			</div>
		</div>
		<div class="col-md-12">
			<hr />
			<p>Pellentesque vestibulum neque sed neque imperdiet consequat at quis dolor. Nam hendrerit turpis ante, non euismod velit tincidunt sit amet. Curabitur eget rhoncus nulla, eget blandit elit. Nam eu diam accumsan nunc elementum porta. Sed augue risus, feugiat ut turpis eget, ullamcorper malesuada velit. Integer enim eros, tincidunt varius enim non, ullamcorper facilisis ex. Etiam lacinia tempor finibus. Donec sagittis tempus ligula non varius. Vivamus luctus eros ut vulputate suscipit. Interdum et malesuada fames ac ante ipsum primis in faucibus. Nullam consectetur consectetur finibus.</p>
		</div>
	</div>
</form>
