<?php

Class ProductsController extends Controller {

	private $productsRepository;

	public function __construct(){
		$this->productsRepository = new ProductsRepository();
	}

	public function defaultAction(){

	}

	public function searchAction(){
		$filters = array();
		if (array_key_exists("text", $_GET)){
			if ($_GET["text"]!=""){
				$filters["text"] = $_GET["text"];
			}
		}
		if (array_key_exists("pricerange", $_GET)){
			if ($_GET["pricerange"]!=""){
				$filters["pricerange"] = $_GET["pricerange"];
			}
		}
		$products = $this->productsRepository->findByFilters($filters);
		include "Views/Products/search.php";
	}

	public function listAction(){
		$itemsPerPage = 2;
		if(array_key_exists("P", $_GET)){
			$page = intval($_GET["P"]);
		} else {
			$page = 1;
		}
		$offset = ($page-1)*$itemsPerPage;

		$products = $this->productsRepository->findAll($itemsPerPage,$offset);
		$numberOfProducts = $this->productsRepository->countByFilters();
		$numberOfPages = ceil($numberOfProducts/$itemsPerPage);

		$view = new View();
		$view->set("products",$products);
		$view->set("numberOfPages",$numberOfPages);
		$view->set("page",$page);
		$view->render(__METHOD__);
	}

	public function editAction(){
		echo "Aici se modifica produsul";
	}

}