<?php
session_start();
include("Helpers/Autoload.php");
include("Helpers/Initialise.php");

define("TEMPLATE",true);
?>
<!DOCTYPE html>
<html>
	<head>
		<?php include "Resources/Public/Partials/HeaderResources.php"; ?>
	</head>
	<body>
		<div class="container">
			<?php include "Resources/Public/Partials/Header.php"; ?>
			<?php initialise(); ?>
			<?php include "Resources/Public/Partials/Footer.php"; ?>
		</div>
		<?php include "Resources/Public/Partials/FooterResources.php"; ?>
	</body>
</html>