<?php 
if (isset($user)) {
?>
	Welcome <?php echo "<b>".$user["firstname"]." ".$user["lastname"]."</b> (".$user["emailaddress"].")"; ?>
	<a href="<?php URL::show("Users","edit");?>">Edit Profile</a> |
	<a href="<?php URL::show("Users","disconnect");?>">Logout</a>
<?php
} else {
?>
	<a href="<?php URL::show("Users","login");?>">Login</a> |
	<a href="<?php URL::show("Users","new");?>">Register</a>
<?php 
}
?>