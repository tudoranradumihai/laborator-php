<?php

function initialise(){

	if(array_key_exists("C", $_GET)){
		$controller = ucfirst($_GET["C"])."Controller";
	} else {
		$controller = "DefaultController";
	}

	if(array_key_exists("A", $_GET)){
		$action = lcfirst($_GET["A"])."Action";
	} else {
		$action = "defaultAction";
	}

	if(class_exists($controller)){
		$object = new $controller();
		if(method_exists($object, $action)){
			$temporary = "$controller::$action";
			$configuration = require "configuration.php";
			if(in_array($temporary, $configuration["administrator"])){
				if(Administrator::check()){
					$object->$action();
				} else {
					echo "ERROR: method can only be accessed by the administrator";
				}
			} else {
				$object->$action();
			}
		} else {
			echo "ERROR: method does not exists";
		}

	} else {
		echo "ERROR: class does not exists.";
	}


}