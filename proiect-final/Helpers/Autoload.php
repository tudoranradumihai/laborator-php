<?php

function __autoload($class){
	$folders = array("Controllers","Models","Models/Repositories","Helpers","Views");
	foreach($folders as $folder){
		$filepath = "$folder/$class.php";
		if(file_exists($filepath)){
			include $filepath;
		}
	}
}