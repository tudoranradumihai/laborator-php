$(document).ready(function(){
	$("#showmore").click(function(){
		$("#pagination").hide();
		
		$.ajax({
  			type: "GET",
  			url: "no-template.php",
  			data: {
  				"C" : "Products",
  				"A" : "list",
  				"P" : $("#page").val()
  			},
		    cache: false,
            success: function(data){
            	alert(data);
     			$(".productslist").append(data);
     			$("#page").val(parseInt($("#page").val())+1);
     			if($("#page").val()==$("#numberOfPages").val()){
     				$("#showmore").hide();
     			}
  			}
		});
	});
	$("#users-new").validate({
		rules: {
			firstname: "required",
			lastname: "required",
			emailaddress: {
				required: true,
				email: true
			},
			password: {
				required: true
			},
			password2: {
				required: true,
				equalTo:  "#password1"
			}
		},
		submitHandler: function(form) {
			form.submit();
	    }
	});

	$("#users-login").validate({
		rules: {
			emailaddress: {
				required: true,
				email: true
			},
			password: {
				required: true
			}
		},
		submitHandler: function(form) {
			form.submit();
	    }
	});
});