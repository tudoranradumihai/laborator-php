<?php
	session_start();
	if(array_key_exists('errors', $_SESSION)){
		foreach ($_SESSION["errors"] as $error) {
		echo $error."<br />";
		}
		unset($_SESSION["errors"]);
	}
	function checkField($key){
		if(array_key_exists("user", $_SESSION)){
			if(array_key_exists($key, $_SESSION["user"])){
				return $_SESSION["user"][$key];
			}
		}
	}
?>
<form method="POST" action="create.php">
 <input type="text" name="fn" placeholder="firstname" value="<?php echo checkField('fn'); ?>">
 <input type="text" name="ln" placeholder="lastname" value="<?php echo checkField('ln'); ?>">
 <input type="text" name="email" placeholder="email" value="<?php echo checkField('email'); ?>">
 <input type="password" name="password" placeholder="">
 <input type="password" name="password2" placeholder="">
 <input type="submit">
</form>
<?php
if(array_key_exists("user", $_SESSION)){
	unset($_SESSION["user"]);
}
?>