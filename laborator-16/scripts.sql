CREATE TABLE users (
	id int(11) PRIMARY KEY AUTO_INCREMENT,
	fn varchar(120) NOT NULL,
	ln varchar(120) NOT NULL,
	email varchar(255) NOT NULL,
	password varchar(255) NOT NULL
);