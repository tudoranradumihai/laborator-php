<?php
function checkAndCreateFolder($folder){
	if (!file_exists($folder)){
		mkdir($folder);
	}
}

// users/T/TR-M-87-10-04.txt

if ($_SERVER["REQUEST_METHOD"]=="POST"){
	$folder = "users";
	checkAndCreateFolder($folder);
	$folder .= "/".strtoupper(substr($_POST["lastname"],0,1));
	checkAndCreateFolder($folder);
	$sex = array(1=>"M",2=>"F");
	$filename = strtoupper(substr($_POST["lastname"],0,1).substr($_POST["firstname"],0,1))."-".$sex[intval(substr($_POST["cnp"],0,1))]."-".substr($_POST["cnp"],1,2)."-".substr($_POST["cnp"],3,2)."-".substr($_POST["cnp"],5,2).".txt";
	$file = fopen($folder."/".$filename,"w");
	fputs($file,json_encode($_POST));
	fclose($file);
}