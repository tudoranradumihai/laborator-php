CREATE DATABASE laboratorphp;

CREATE TABLE orders (
	id INT(11) PRIMARY KEY AUTO_INCREMENT,
	firstname VARCHAR(80),
	lastname VARCHAR(80),
	emailaddress VARCHAR(120) NOT NULL DEFAULT "CACA.RO",
	address TEXT,
	product VARCHAR(200),
	price FLOAT(9,2),
	createddate DATETIME
);

INSERT INTO orders (firstname, lastname, 
					emailaddress, address, 
					product, price, createddate) 
		VALUES ('Radu Mihai', 'Tudoran', 
			'radu.tudoran@pitechnologies.ro', 'Cluj Napoca', 
			'TV -120`', 99.10 , '2017-11-07 02:02:02');

INSERT INTO orders (firstname, lastname, 
					emailaddress, 
					product, price) 
		VALUES ('Radu Mihai', 'Tudoran', 
			'radu.tudoran@pitechnologies1.ro',
			'TV -120`', 99.10), 
				('Radu Mihai', 'Tudoran', 
				'radu.tudoran@pitechnologies2.ro',
				'TV -120`', 99.10);

INSERT INTO orders (firstname, lastname,  Product, price) 
		VALUES ('Radu Mihai', 'Tudoran', 'TV -120`', 99.10)

SELECT * FROM orders;
SELECT firstname, lastname FROM orders;
SELECT * FROM orders WHERE id=1;
SELECT * FROM orders WHERE emailaddress LIKE 'CACA.RO';
SELECT * FROM orders WHERE emailaddress LIKE 'radu.tudoran@pitechnologies221.ro';
SELECT * FROM orders WHERE firstname LIKE 'Ra%';
SELECT * FROM orders WHERE address LIKE '%cluj%';
SELECT * FROM orders WHERE price = 99.10;

UPDATE orders SET emailaddress = 'mihai.tudoran@gmail.com' WHERE id=1;
UPDATE orders SET emailaddress = 'mihai@gmail.com', address = 'IASI' 
				WHERE id=1 OR id = 2;

ALTER TABLE orders CHANGE firstname fistname VARCHAR(84);























