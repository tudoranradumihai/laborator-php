<?php
/*
se vau doua stringuri 1 ce contine toate vocalele

$vocale = "aeiou";
...

Din aceste doua variabile se vor genera cuvinte random dupa cum urmeaza
un cuvant este un string intre 2 si 8 litere
ce poate incepe cu vocala sau consoana, nu poate incepe cu doua vocale 
nu poate contine mai mult de doua consoane lipite

exemplu corect: wase
incorect : aesered
incorect: wasz

acest sistem de generare se va insera intr-o functie 
functia respectiva se va apela in alta functie pentru a genera o propozitie.

vom genera propozitii continand intre 3 si 15 cuvinte fiecare.

si ultima functie este un generator de paragrafe care va face intre 2 si 5 paragrafe continand intre 8 si 12 propozitii.
*/


function generateWord(){

	$vocale = "aeiou";
	$consoane = "bcdfghjklmnpqrstvwxyz"; 

	$litere = array("vocale","consoane");

	$lungimeCuvant = rand(2,8);

	$cuvant = "";

	for($i=0;$i<$lungimeCuvant;$i++){
		if (strlen($cuvant)==0){
			$rand = rand(0,1);
			$cuvant = ${$litere[$rand]}[rand(0,strlen(${$litere[$rand]})-1)];
			
		} else {
			if (strpos($vocale,$cuvant[strlen($cuvant)-1])!==false){
				$cuvant .= $consoane[rand(0,strlen($consoane)-1)];
			} else {
				$cuvant .= $vocale[rand(0,strlen($vocale)-1)];
			}
		}
	}
	return $cuvant;
}

echo generateWord();