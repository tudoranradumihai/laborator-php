<?php

function __autoload($class){

	$folders = array(
		"Dosar",
		"Dosar2"
	);

	foreach ($folders as $folder){
		$path = $folder."/".$class.".php";
		if (file_exists($path)){
			include $path;
		}
	}

}

$object1 = new A();

$object2 = new B();

$object3 = new C();

echo $object2->prop;