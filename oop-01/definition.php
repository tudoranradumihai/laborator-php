<?php
/*
Class ClassName {
	$prop1;
	$prop2;
	$prop3;
} 
*/

// declararea clasei
Class Box {
	// declararea proprietatilor publice
	public $length;
	public $width;
	public $height;

	function showMessage(){
		echo "This is a box";
	}

	function returnVolume(){
		$volume = $this->length * $this->width * $this->height;
		return $volume;
	}

	function showVolume(){
		echo self::returnVolume();
	}
}

// crearea obiectului de tip Class Box
$object = new Box();

// asignarea valorii unei proprietati publice
$object->length = 100;
$object->width  = 50;
$object->height = 30;

// afisarea valorii unei proprietati publice
//echo $object->length;

$object->showMessage();
echo "<br />";
echo $object->showVolume();