<?php

Class UsersController {

	public function listAction(){
		$users = Users::findAll();
		include "Views/Users/list.php";
	}

	public function newAction(){
		include "Views/Users/new.php";
	}

}