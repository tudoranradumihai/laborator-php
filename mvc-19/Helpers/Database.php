<?php

Class Database {

	private $connection;

	public function __construct(){
		$this->connection = mysqli_connect("localhost","root","","gcl19");
	}

	public function executeQuery($query){
		return mysqli_query($this->connection,$query);
	}

	public function __destruct(){
		mysqli_close($this->connection);
	}

}