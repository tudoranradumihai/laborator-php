<?php

function __autoload($className){
	$folders = array(
		"Controllers",
		"Models",
		"Helpers"
	);
	foreach ($folders as $folder){
		$filePath = $folder."/".$className.".php";
		if (file_exists($filePath)){
			include($filePath);
		}
	}

}