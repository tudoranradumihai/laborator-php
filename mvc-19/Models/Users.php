<?php

Class Users {

	public $id;
	public $firstname;
	public $lastname;

	public function __construct($id,$firstname,$lastname){
		$this->id = $id;
		$this->firstname = $firstname;
		$this->lastname = $lastname;
	}

	public static function findAll(){
		$database = new Database();
		$result = $database->executeQuery("SELECT * FROM users");
		$array = array();
		while($line = mysqli_fetch_array($result)){
			$object = new Users($line["id"],$line["firstname"],$line["lastname"]);
			array_push($array,$object);
		}
		return $array;
	}

}