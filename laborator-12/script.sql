/* creare tabel */
CREATE TABLE users(
	id INT(11) PRIMARY KEY AUTO_INCREMENT,
	firstname VARCHAR(100) NOT NULL,
	lastname VARCHAR(100) NOT NULL,
	emailaddress VARCHAR(120) NOT NULL,
	address VARCHAR(200),
	telephone VARCHAR(20),
	createddate DATETIME DEFAULT CURRENT_TIMESTAMP, /* se completeaza la insert */
	updateddate DATETIME ON UPDATE CURRENT_TIMESTAMP /* se completeaza la update */
);

/*
PRIMARY KEY - se stabileste cheia primara a tabelului.
AUTO_INCREMENT - in cazul in care nu se introduce valoare in field-ul respectiv, se va asigna automat urmatoarea valoare numerica disponibila.
NOT NULL - in cazul in care se incearca introducerea de date fara a adauga valoare intr-un field NOT NULL, se va returna eroare.
DEFAULT - valoarea din oficiu care se insereaza daca nu se introduce valoare in field-ul respectiv.
CURRENT_TIMESTAMP - valoarea de tip data si timp din momentul in care se executa operatiunea
*/

/* modificare tabel adaugare coloana la sfarsitul structurii */
ALTER TABLE users ADD gender SMALLINT;

/* modificare tabel adaugare coloana in interiorul structurii */
ALTER TABLE users ADD gender SMALLINT AFTER lastname;

/* modificare tabel modificare coloana*/
ALTER TABLE users MODIFY COLUMN address TEXT;

/* modificare tabel stergere coloana */
ALTER TABLE users DROP COLUMN gender;


/* stergere continut  tabel si resetare cheie primara la 1 */
TRUNCATE TABLE users;

/* stergere tabel*/
DROP TABLE users;

/* introducere informatii in tabel */
INSERT INTO users (firstname,lastname,emailaddress) VALUES ('Radu','Tudoran','radu.tudoran@academyplus.ro');

/* introducere informatii multiple in tabel*/
INSERT INTO users (firstname,lastname,emailaddress) VALUES ('Radu','Tudoran','radu.tudoran@academyplus.ro'),('Adrian','Rotar','office@ria-events.ro');
