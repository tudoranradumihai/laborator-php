<?php

$hostname = "localhost";
$username = "root";
$password = "";
$database = "laborator-12";

// http://php.net/manual/en/function.mysqli-connect.php
$connection = mysqli_connect($hostname,$username,$password,$database);
if (!$connection) {
    echo "Error: Unable to connect to MySQL." . PHP_EOL;
    echo "Debugging errno: " . mysqli_connect_errno() . PHP_EOL;
    echo "Debugging error: " . mysqli_connect_error() . PHP_EOL;
    exit;
}

$query = "INSERT INTO users (firstname,lastname,emailaddress) VALUES ('Radu','Tudoran','radu.tudoran@academyplus.ro');";

$result = mysqli_query($connection,$query);
if ($result){
	echo "SUCCESS";
}

// http://php.net/manual/en/mysqli.close.php
mysqli_close($connection);