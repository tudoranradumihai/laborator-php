<?php

$users = array(
	array(
		"lastname" => "Tudoran",
		"firstname" => "Radu",
		"sex" => "M",
		"birthdate" => "04.10.1987"
	),
	array(
		"lastname" => "Rotar",
		"firstname" => "Adrian",
		"sex" => "M",
		"birthdate" => "30.01.1997"
	),
	array(
		"lastname" => "Grigorescu",
		"firstname" => "Rares",
		"sex" => "M",
		"birthdate" => "14.08.1988"
	),
	array(
		"lastname"=> "Boia",
		"firstname"=> "Marius",
		"sex"=> "M",
		"birthdate"=> "16.05.1982"
	),
	array(
		"lastname" => "Zsizsmann",
		"firstname" => "Endre",
		"sex" => "M",
		"birthdate" => "16.01.1989"
	),
	array(
		"lastname" => "Alkhzouz",
		"firstname" => "Imola",
		"sex" => "F",
		"birthdate" => "25.09.1990"
	),
	array(
		"lastname" => "Maniutiu",
		"firstname" => "Cristian",
		"sex" => "M",
		"birthdate" => "03.29.1986"
	),
	array(
		"lastname" => "Kocz",
		"firstname" => "Bogdan",
		"sex" => "M",
		"birthdate" => "25.02.1985"
	),
	array(
        "lastname" => "Horgos",
        "firstname" => "Ionut",
        "sex" => "M",
        "birthdate" => "01.15.1994"
    ),
    array(
		"lastname"=>"Tripon",
		"firstname"=>"Adrian",
		"sex"=>"M",
		"birthdate"=>"23.11.1984"
	),
	array(
		"lastname" => "Moldovan",
		"firstname" => "Sergiu",
		"sex" => "M",
		"birthdate" => "02.10.1993"
	)
);