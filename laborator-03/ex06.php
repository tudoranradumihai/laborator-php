<?php

$a = array();
var_dump($a);
echo "<br />";

$b = array(1,2,3,4,5);
var_dump($b);
echo "<br />";

$c = array(NULL,false,1,2.5,"text");
$c[3] = 2.6;
$c[] = 10;
$d = 10;
$c[$d] = "alt text";
$c[] = "ultimul element";
var_dump($c);
echo "<br />";

$d = array();
$d[10] = "zece";
$d[20] = "2zece";
$d[30] = "3zece";
var_dump($d);
echo "<br />";

$e = array(
	10 => "zece",
	20 => "2zece",
	30 => "multezece"
);

echo $e[20];
echo "<br />";

$f = array();
$f["name"] = "John";
$f["email"] = "john.doe@domain.com";
$f[] = "CEVA";
var_dump($f);
echo $f["name"];
echo $f[0];

$g = array(
	"name" => "John",
	"email" => "adresa lu john"
);