<?php
include("Helpers/Autoload.php");

define("TEMPLATE",true);
?>


<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
</head>
<body>
	<div class="container">
		<h1>E MAG</h1>
		<nav class="navbar navbar-default">
  			<div class="container-fluid">
  				<ul class="nav navbar-nav">
        			<li><a href="index.php?C=Products&A=list">All Products</a></li>
        			<li><a href="index.php?C=Products&A=search">Search</a></li>
        		</ul>
  			</div>
  		</nav>

	<?php
		if (array_key_exists("C", $_GET)){
			$controller = $_GET["C"]."Controller";
		} else {
			$controller = "DefaultController";
		}

		if (array_key_exists("A", $_GET)){
			$action = $_GET["A"]."Action";
		} else {
			$action = "defaultAction";
		}

		$object = new $controller();
		$object->$action();

	?>
	</div>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script src="Resources/Public/JS/custom.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</body>
</html>


