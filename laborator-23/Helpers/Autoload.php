<?php

function __autoload($class){
	$folders = array("Controllers","Helpers","Models","Models/Repositories","Views");
	foreach($folders as $folder){
		$filename = $folder."/".$class.".php";
		if(file_exists($filename)){
			include $filename;
		}
	}
}