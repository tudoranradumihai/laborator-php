$(document).ready(function(){
	$("#showmore").click(function(){
		$("#pagination").hide();
		
		$.ajax({
  			type: "GET",
  			url: "no-template.php",
  			data: {
  				"C" : "Products",
  				"A" : "list",
  				"P" : $("#page").val()
  			},
		    cache: false,
            success: function(data){
            	alert(data);
     			$(".productslist").append(data);
     			$("#page").val(parseInt($("#page").val())+1);
     			if($("#page").val()==$("#numberOfPages").val()){
     				$("#showmore").hide();
     			}
  			}
		});
	});
});